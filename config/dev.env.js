'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  LOGIN_HOST: '"http://login.macrm.tk"',
  REG_PATH: '"/reg"',
  CONFIRM_PATH: '"/confirm"',
  LOGIN_PATH: '"/login"',
  CHECK_PATH: '"/check"',
})
