'use strict'
module.exports = {
  NODE_ENV: '"production"',
  LOGIN_HOST: '"http://login.macrm.tk"',
  REG_PATH: '"/reg"',
  CONFIRM_PATH: '"/confirm"',
  LOGIN_PATH: '"/login"',
  CHECK_PATH: '"/check"',
}
