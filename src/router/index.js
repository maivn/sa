import Vue from 'vue'
import Router from 'vue-router'
import AppMain from '@/components/AppMain'
import Login from '@/components/forms/Login'
import Registration from '@/components/forms/Registration'
import Reg from '@/components/forms/Login/Reg'
import Confirm from '@/components/forms/Login/Confirm'
import Restore from '@/components/forms/Restore'
import Test from '@/components/forms/Test'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'AppMain',
      component: AppMain,
      beforeEnter: (to, from, next) => {
        console.log("beforeEnter main")
        var auth = {
          main: false
        }
        if (auth.main) {
          next()
        } else {
          next({
            path: '/login'
          })
        }
      },
    },
    {
      path: '/test',
      component: Test
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/reg',
      component: Reg
    },
    {
      path: '/confirm',
      component: Confirm
    },
    {
      path: '/registration',
      component: Registration
    },
    {
      path: '/restore',
      component: Restore
    }
  ]
})
