// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import VeeValidate, {Validator} from 'vee-validate'

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.withCredentials = true;

const dictionary = {
  custom: {
    name: {
      required: () => 'Укажите имя',
      min: 'Проверьте правильность своего имени'
      // custom messages
    },
    email: {
      required: () => 'Укажите электронную почту',
      email: 'Неизвестный формат. Убедитесь, что адрес указан верно'
      // custom messages
    },
    login: {
      required: () => 'Укажите электронную почту',
      email: 'Неизвестный формат. Убедитесь, что адрес указан верно'
    },
    phone: {
      required: () => 'Укажите номер телефона',
      min: 'Неизвестный формат. Убедитесь, что номер указан верно'
      // custom messages
    },
    code: {
      required: () => 'Укажите код подтверждения',
      min: 'Убедитесь, что код указан верно',
      max: 'Убедитесь, что код указан верно'
      // custom messages
    },
    password: {
      required: () => 'Укажите пароль',
      min: 'Пароль не должен быть меньше 6 символов'
      // custom messages
    },
    pw_confirm: {
      required: () => 'Укажите пароль',
      confirmed: () => 'Пароли не совпадают'
      // custom messages
    }
  }
}

Validator.localize('ru', dictionary);
Vue.use(VeeValidate)
Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})


